# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-02-14 13:22
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tweet',
            name='put_date',
            field=models.DateField(default=datetime.date(2017, 2, 14)),
        ),
    ]
