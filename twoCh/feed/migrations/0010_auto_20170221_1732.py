# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-02-21 14:32
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0009_tweet_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='put_date',
            field=models.DateTimeField(default=datetime.datetime.today),
        ),
    ]
