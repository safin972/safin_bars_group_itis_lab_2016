from itertools import chain
from operator import attrgetter


from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import ugettext as _

from feed.form import SettingsForm, TweetForm
from feed.models import Tweet, Retweet, Follow, Like
import json


# Create your views here.

@login_required
def home (request):
    if request.method == "POST":
        t = Tweet(user=request.user)
        f=TweetForm(request.POST,request.FILES,instance=t)
        f.save()
        return HttpResponseRedirect(reverse("feed:home"))
    t=TweetForm()
    tweets = Tweet.objects.all()
    retweets = Retweet.objects.all()
    tweets_n_retweets = sorted(chain(tweets, retweets), key=attrgetter('pub_date'), reverse=True)
    return render(request, "feed/home.html", {"items": tweets_n_retweets,'f':t},)


@login_required
def list_tweets(request,**kwargs):
    # tweets=Tweet.objects.filter(user__username=username).order_by("-pub_date")
    # return render(request,"feed/list_tweets.html", {'tweets':tweets})
    follow_list = Follow.objects.filter(follower__username=kwargs['username'])
    state = None
    folower=User.objects.get(username=kwargs['username'])
    for follow in follow_list:
        if follow.followee == request.user:
            state = _("You have read this user")
    if folower == request.user:
        state = _("This is your page")
    tweets = User.objects.get(username=kwargs['username']).tweet_set.order_by("-pub_date")
    return render(request, "feed/list_tweets.html", {"tweets": tweets,"username":kwargs['username'],"state":state,
                                                     'like': like})

@login_required
def cite(request):
    if request.method != "POST":
        return HttpResponse(_("Not supported"))
    t = Tweet(user=request.user)
    origin = Tweet.objects.get(id=request.POST["tweet_id"])
    t.text = 'RT @%s: "%s" on %s' % (origin.user.username,
                                     origin.text,
                                     origin.pub_date)
    t.save()
    return HttpResponseRedirect(reverse("feed:home"))


def retweet(request):
    if request.method != "POST":
        return HttpResponse(_("Not supported"))
    rt = Retweet(
        user=request.user,
        tweet_id=request.POST["tweet_id"]
    )
    rt.save()
    return HttpResponseRedirect(reverse("feed:home"))


def add_follower(request):
    if request.method != "POST":
        return HttpResponse (_("Not supported"))
    username=request.POST['user_name']
    follower=User.objects.get(username=username)
    follow = Follow(follower=follower,followee=request.user)
    follow.save()
    return HttpResponseRedirect (reverse("feed:list_tweets",kwargs={'username':username}))


def like(request):
    if request.method != "POST":
        return HttpResponse (_("Not supported"))
    tweet=Tweet.objects.get(id=request.POST['tweet_id'])
    try:
        like=Like.objects.get(liker=request.user,tweet=tweet)
        like.delete()
        like="e"
    except Exception:
        like=Like(liker=request.user,tweet=tweet)
        like.save()
    return HttpResponseRedirect(reverse('feed:list_tweets',kwargs={'username':tweet.user.username}))


def unfollow(request):
    user=User.objects.get(username=request.POST['user_name'])
    if request.method != "POST":
        return HttpResponse (_("Not supported"))
    try:
        follow=Follow.objects.get(follower=user,followee=request.user)
        follow.delete()
    except Exception:
        pass
    return HttpResponseRedirect(reverse('feed:list_tweets', kwargs={'username': user.username}))

def login (request):
    return render(request,'login.html')

def head_page (request):
    user=request.user
    print(user)
    return render(request,'feed/headPage.html',{'user':user})

def settings(request):
    if request.method=="POST":
        f=SettingsForm(request.POST)
        if f.is_valid():
            return HttpResponse(str(f.cleaned_data))
        else:
            return render(request, 'feed/settings.html', {'form': f})
    else:
        f=SettingsForm()
    #f.prefix="settings"

        return render(request,'feed/settings.html',{'form':f})
def ajax_get_users (request):
    # json_objects=serialize('json',User.objects.all(),fields=("username"))
    # return JsonResponse(json.loads(json_objects),safe=False)
    return HttpResponse("POST")