from django.conf.urls import url

from feed import views

urlpatterns = [
    url(r'^$',views.home,name='home'),
    url(r'user/(?P<username>\w+)', views.list_tweets,name='list_tweets'),
    url(r'^cite$', views.cite, name="cite"),
    url(r'^retweet$', views.retweet, name="retweet"),
    url(r'^follow$', views.add_follower, name="follow"),
    url(r'^unfollow$', views.unfollow, name="unfollow"),
    url(r'^like$', views.like, name="like"),
    url(r'^head$', views.head_page, name="head"),
    url(r'^settings$', views.settings, name="settings"),
    url(r'^ajax_get_users$', views.ajax_get_users, name="ajax_get_users"),

]
