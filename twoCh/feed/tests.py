from django.contrib.auth.models import User
from django.test import Client
from django.test import TestCase

# Create your tests here.
from django.urls import reverse


class FeedTest (TestCase):

    def setUp(self):
        self.c=Client()

    def testAnnoHasNoAccesToFeed(self):
        r= self.c.get(reverse('feed:home'))
        self.assertEquals(302,r.status_code)

    def testUserHasAccesToFeed(self):
        u=User.objects.create_user(username='user',password='user')
        self.c.login(username='user',password='user')
        r=self.c.get(reverse('feed:home'))
        self.assertEquals(200,r.status_code)

    def testPageFeedisFeed(self):
        u = User.objects.create_user(username='user', password='user')
        self.c.login(username='user', password='user')
        r = self.c.get(reverse('feed:home'))
        print(str(r))
