from django.contrib import admin
from feed.models import Tweet, Retweet, Follow, Like

# Register your models here.
@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
    list_display = ('user','pub_date','text')
    search_fields = ('text',)
    list_filter = ('pub_date',)
    date_hierarchy = 'pub_date'
    ordering = ('-pub_date',)
    empty_value_display = '-empty-'
    fieldsets = (
        ('Main options',{'classes':('wide',),'fields':('user','pub_date')}),
        ('Advanced options',{'classes':('wide',),'fields':('text',)})
    )
    readonly_fields = ('text',)
    view_on_site = True

    radio_fields = {'user':admin.HORIZONTAL}
    save_as = True
    def get_queryset(self, request):
        if request.user.is_superuser:
            return Tweet.objects.all()
        return Tweet.objects.filter(user=request.user)


admin.site.register(Retweet)
admin.site.register(Follow)
admin.site.register(Like)
