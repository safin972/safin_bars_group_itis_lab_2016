import datetime

from django.contrib.auth.models import User
from django.db import models



# Create your models here.


class GeneralTweet(models.Model):
    class Meta:
        abstract = True
    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
    pub_date = models.DateTimeField(default=datetime.datetime.today,verbose_name="Publication date")


class Retweet(GeneralTweet):
    tweet = models.ForeignKey("Tweet")
    def isRT(self):
        return True


class Tweet(GeneralTweet):
    text = models.TextField()
    image=models.ImageField(blank=True)
    def isRT(self):
        return False


    def __str__(self):
        return '%s : %s : %s' % (self.user,self.pub_date,self.text)

class Follow (models.Model):
    follower=models.ForeignKey(User,related_name="follower")
    followee=models.ForeignKey(User,related_name="followee")
    follow_date=models.DateTimeField(default=datetime.datetime.today)

class Like(models.Model):
    liker = models.ForeignKey(User, on_delete=models.CASCADE, related_name='liker')
    tweet = models.ForeignKey(Tweet)
    def __str__(self):
        return "%s" % (self.liker.username)