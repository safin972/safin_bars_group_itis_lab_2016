import urllib
from urllib.request import urlretrieve
from bs4 import BeautifulSoup
import re
import json


doc=urllib.request.urlopen('http://kpfu.ru/main?p_id=30188&p_lang=&p_type=8')
html_code=doc.read().decode('1251')
soup= BeautifulSoup(html_code,'html.parser')
days_with_html=re.findall('padding-top:5px\W{3}\w+',html_code)
for_json={}
days=[]
for day in days_with_html:
    days.append(day.split('>')[1])
for table in soup.findAll('table'):
    i=0
    times=re.findall('\d{2}:\d{2}-\d{2}:\d{2}',str(table))
    subjects=table.findAll('strong')
    time_subject_list={}
    for time in times:
        for subject in subjects:
            subject=str(subject).replace('<strong>','')
            time_subject_list[time]=subject.replace('</strong>','')
    if (i<len(days)):
        for_json[days.pop(i)]=time_subject_list
    i+=1
print(for_json)


