# coding=utf-8
from threading import Thread
from queue import Queue
from urllib.request import urlretrieve

q=Queue()
list=["https://www.yandex.ru/","http://kpfu.ru/"]

class Producer (Thread):

    def __init__(self,list):
        Thread.__init__(self)
        self.list=list

    def put(self):
        for i in self.list:
            q.put(i)

    def run(self):
        Producer.put(self)


class Consumer (Thread):

    def __init__(self):
        Thread.__init__(self)

    def run(self):
        url=q.get()
        name=url.split("/")
        urlretrieve(url,name[-2]+'.html')
        print("Downloaded: {0}".format(url))

t1=Producer(list)
t2=Producer(list)
t3=Consumer()
t4=Consumer()
t3.start()
t4.start()
t1.start()
t2.start()
t1.join()
t2.join() 
t3.join()
t4.join()