# coding=utf-8

import  os
from datetime import datetime
import time

input_line = input()
directory = "C:/Users/kair4/literatura"
commands = input_line.split(" ")
first_date = datetime.strptime(commands[0],'%b-%d-%Y')
second_date = datetime.strptime(commands[1],'%b-%d-%Y')
first_date_sec = time.mktime(first_date.timetuple())
second_date_sec = time.mktime(second_date.timetuple())
list=()
for root, dirs, files in os.walk(directory):
    for i in range(len(files)):
        if (files[i].endswith("pdf")):
            f_directory=directory+"/"+files[i]
            f_date_sec = os.path.getmtime(f_directory)
            if (first_date_sec < f_date_sec and second_date_sec > f_date_sec):
                print(files[i])
