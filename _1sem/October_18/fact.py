# coding=utf-8

from xrange import  xrange

def fact (a):
    result=1;
    if (isinstance(a,int)):
        if(a>0):
            for i in xrange(1,a+1,1):
                result*=i
        elif (a==0):
            return 1
        else:
            raise TypeError
        return result
    else:
        raise TypeError