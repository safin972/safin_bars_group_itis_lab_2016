from django import forms

from first.models import RunHistory


class ArgsForm (forms.ModelForm):
    class Meta:
        model=RunHistory
        fields=['arg1','arg2']