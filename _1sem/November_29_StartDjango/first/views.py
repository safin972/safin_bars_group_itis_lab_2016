import re

import datetime
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render

from first.forms import ArgsForm
from first.models import RunHistory


def sum (request):
    data={}
    if 'arg1' in request.POST and 'arg2' in request.POST:
        a=int(request.POST['arg1'])
        b=int(request.POST['arg2'])
        data = {'result':a+b}
        r=RunHistory(arg1=a,arg2=b,datetime=datetime.datetime.now())
        r.save()
    data['form']=ArgsForm()
    return render(request,'index.html',data)


def showresult (request):
    a=RunHistory.objects.order_by('-datetime')
    return render(request,'result.html',{'resultset':a})

