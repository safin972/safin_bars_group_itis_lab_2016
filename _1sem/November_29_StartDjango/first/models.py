from django.db import models
import datetime

class RunHistory (models.Model):
    arg1=models.IntegerField()
    arg2=models.IntegerField()
    datetime = models.DateField(default=datetime.datetime.now())

    def __str__(self):
        return '%s + %s : %s'%(self.arg1,self.arg2,self.datetime)

