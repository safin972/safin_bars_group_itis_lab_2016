# coding=utf-8

import os
import shutil
import urllib
from urllib.request import urlretrieve

import time
from bs4 import BeautifulSoup



jj=[]
working=True
while working:
    line=input(os.getcwd()+"> ")
    line_args=line.split(" ")
    if (line_args[0]=='exit'):
        working=False
        continue
    elif(line_args[0]=='ls' and len(line_args)==1):
        print(os.listdir(os.getcwd()))
    elif (line_args[0] == 'ls' and len(line_args) != 1):
        try:
            print(os.listdir(line_args[1]))
        except FileNotFoundError:
            print('Директория не найдена')
    elif (line_args[0] == 'cd' and len(line_args) == 1):
        print(os.listdir(os.getcwd()))
    elif (line_args[0] == 'cd' and len(line_args) != 1):
        try:
            os.chdir(line_args[1])
        except FileNotFoundError:
            print('Директория не найдена')
    elif (line_args[0] == 'supercopy' and len(line_args) == 1):
        print("Пропиши имя файла")
    elif (line_args[0] == 'supercopy' and len(line_args) != 1):
        try:
            j=int(line_args[2])
            for i in range(j):
                jj=line_args[1].split('.')
                shutil.copy(line_args[1],os.getcwd()+'CopyFail'+str(i)+'.'+jj[-1])
        except ValueError:
            "не верно введены данные"
    elif (line_args[0]== "wget" and len(line_args)!=1):
        if (line_args[-1].endswith("jpg")):
            try:
                print("Starting download..")
                url=line_args[1]
                flag=True
                for i in line_args:
                    if (i==">"):
                        name=line_args[-1]
                        logo = urlretrieve(url, name)
                        flag=False
                if(flag==True):
                    name = url.split("/")
                    logo = urlretrieve(url, name[-1])
                print("Done.")
            except ValueError:
                "не верно введены данные"
        else:
            doc=urllib.request.urlopen(line_args[1])
            html_code=doc.read()
            flag1=True
            for i in line_args:
                if (i==">"):
                    flag1=False
            if(flag1==False):
                html_name=line_args[-1]
                line_args[-1]=line_args[1]
                print("Download page")
                file_paths=html_name.split(".")
                file_path=os.getcwd()+"\{0}".format(file_paths[0])
                os.mkdir(file_path)
                os.chdir(file_path)
                html_file=urlretrieve(line_args[1],html_name)
            else:
                print("Download page")
                html_file=urlretrieve(line_args[1],"page{0}.html".format(time.time()))
            soup=BeautifulSoup(html_code,"html.parser")
            print("Starting components download..")
            for at_link in soup.find_all("link"):
                link=at_link.get("href")
                if (link.endswith(".css")):
                    css_link=link[1:]
                    css_name=css_link.split("/")
                    print("Downloading {0}".format(css_name[-1]))
                    css_file_link=line_args[-1]+css_link
                    css_file=urlretrieve(css_file_link,css_name[-1])
                if (link.endswith(".js")):
                    js_link = link[1:]
                    js_name=js_link.split("/")
                    print("Downloading {0}".format(js_name[-1]))
                    js_file_link = line_args[-1] + js_link
                    js_file = urlretrieve(js_file_link, js_name[-1])
            for at_img in soup.find_all("img"):
                src_link=at_img.get("src")
                img_link=src_link[1:]
                img_name = img_link.split("/")
                print("Downloading {0}".format(img_name[-1]))
                img_file=urlretrieve(line_args[1]+img_link,img_name[-1])
            print("Done.")