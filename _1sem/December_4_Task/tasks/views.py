import time
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import F
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseRedirect as Redirect
from django.shortcuts import render

# Create your views here.
from tasks.forms import EditTaskForm, TaskForm
from tasks.models import Task


def show_tasks (request):
    # task_list=Task.objects.all()
    # result="<br>".join([task.text for task in task_list])
    #
    # task_list = Task.objects.values_list("text",flat=True)
    # result = "<br>".join(task_list)
    # q1=Q(text__contains="Do")
    # q2 = Q(priority__gt=2)
    # q=q1|q2
    # task_list=Task.objects.filter(q)
    #task_list = Task.objects.filter(time_given__lt=F("deadline"))
    #result = "<br>".join([task.text for task in task_list])
    #return HttpResponse(result)
    return render(request,
                  "all_tasks.html",
                  {"tasks": Task.objects.all()})

def show_task (request,task_id):
    t=Task.objects.get(id=task_id)
    return HttpResponse ("<h1>Task # %s</h1>" %t)



def create_task(request):
    if request.user.is_authenticated():
        context = {}
        if 'text' in request.POST:
            f = TaskForm(request.POST)
            f.save()
            context["result"] = "Task created"
            return Redirect(reverse("tasks:all"))
        context["form"] = TaskForm()
        return render(request, "Add.html", context)
    else:
        return Redirect(reverse('login'))

@login_required(login_url=reverse_lazy('login'))
def edit_task(request, task_id):

    if "edit10" in request.session:
        if time.time() - request.session["edit10"]<10:
            return Redirect(reverse('tasks:all'))
        else:
            request.session["edit10"] = time.time()
    else:
        request.session["edit10"] = time.time()
    if request.method == "POST":
        task = Task.objects.get(id=task_id)
        f = EditTaskForm(request.POST, instance=task)
        f.save()
        return Redirect(reverse("tasks:all"))

    elif request.method == "GET":
        task = Task.objects.get(id=task_id)
        f = EditTaskForm(instance=task)
        return render(request,
                      "Edit.html",
                      {"form": f})
    else:
        return HttpResponse("Method is not supported")


def log_in(request):
    if request.method=='POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password = password)
        if (user is not None):
            auth_login(request,user)
            return Redirect(reverse("tasks:all"))
        else:
            return Redirect (reverse('login'))
    else:
        return render(request,"log_in.html")