# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-12-06 15:05
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_auto_20161206_1744'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='priority',
            field=models.IntegerField(default=5),
        ),
        migrations.AlterField(
            model_name='task',
            name='deadline',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2016, 12, 6, 18, 5, 47, 26567), null=True),
        ),
    ]
