from unittest import TestCase

from Player import Player


class TestPlayer(TestCase):

    def setUp(self):
        self.player=Player()
        self.enemy=Player()

    def test_simple_kick(self):
        oldhp=self.enemy.hp
        self.player.simple_kick(self.enemy)
        self.assertEquals(oldhp,self.enemy.hp)

    @classmethod
    def setUpClass(cls):
        cls.p=Player()

    def test_player_has_hp (self):
        self.assertTrue(hasattr(self.p,'hp'))

    def test_player_has_name(self):
        self.assertTrue(hasattr(self.p,'name'))